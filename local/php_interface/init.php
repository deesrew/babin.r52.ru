<?
/*
* добавляем пользователя в группы
* 9 - Физ.Лицо
* 10 - Партнер
*/
AddEventHandler("main", "OnBeforeUserRegister", Array("EventHandlerCaptureClass", "OnBeforeUserRegisterHandler"));
class EventHandlerCaptureClass
{
	function OnBeforeUserRegisterHandler(&$arFields)
	{
        if (strlen($_REQUEST["REGISTER"]["IS_PARTNER"]) > 0) {
            $arFields["GROUP_ID"][] = 10;
        } else {
            $arFields["GROUP_ID"][] = 9;
        }
	}
}
