<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arTemplateParameters = array(
	"USER_PROPERTY_NAME"=>array(
		"NAME" => GetMessage("USER_PROPERTY_NAME"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"ADD_USER_TO_PARTNER_GROUP" => Array(
		"NAME" => "Добавлять пользователя в группу Партнеры",
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	)
);
?>
