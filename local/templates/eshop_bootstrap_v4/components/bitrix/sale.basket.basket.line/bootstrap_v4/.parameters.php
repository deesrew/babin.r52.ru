<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"SHOW_REGISTRATION_PARTNER" => Array(
        "PARENT" => "AUTHOR",
		"NAME" => "Показывать партнерский клуб",
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"PATH_TO_REGISTER_PARTNER" => Array(
        "PARENT" => "AUTHOR",
		"NAME" => "Страница авторизации партнера",
		"TYPE" => "STRING",
		"DEFAULT" => "={SITE_DIR.\"login-partner/\"}",
	)
);
?>
